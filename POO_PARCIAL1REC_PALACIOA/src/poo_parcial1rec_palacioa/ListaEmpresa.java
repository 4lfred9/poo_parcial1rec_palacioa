package poo_parcial1rec_palacioa;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author 4lfred
 */
public class ListaEmpresa {

    Scanner sc = new Scanner(System.in);
    Empresa empresa = new Empresa();
    ArrayList<Empresa> listaEmpresa = new ArrayList();
    //ListaCliente lcli = new ListaCliente();
    Cliente cli = new Cliente();
    //ListaEmpleado lemp = new ListaEmpleado();
    Empleado empp = new Empleado();

    public void agregarEmpresa() {
        String resp = "s";
        while (resp.equals("s")) {
            System.out.println("INGRESE NOMBRE DE EMPRESA");
            String nomEmpr = sc.nextLine();
            empresa = new Empresa();
            empresa.setNomEmpresa(nomEmpr);
            listaEmpresa.add(empresa);
            System.out.println("Continuar: s/n");
            resp = sc.nextLine();
        }
    }

    public void listarEmpresa() {
        // listaEmpresa.add(empresa);
        // listaEmpresa.add( = new Empresa(emp, cli, nomEmpresa));
        //listaEmpresa.add(emp = new Empleado(1, "SIMPSON HOMERO", "VENDEDOR", 10000, 0));
        System.out.println("----EMPRESAS----");
        int cont = 0;
        for (int i = 0; i < listaEmpresa.size(); i++) {
            //empresa.mostrarEmpresas();
            cont = cont + 1;
            System.out.println(cont + "- " + listaEmpresa.get(i).getNomEmpresa());
        }
    }

    public void AgregarCliente() {
        System.out.println("EMPRESAS: ");
        int cont = 0;
        for (int i = 0; i < listaEmpresa.size(); i++) {
            cont = cont + 1;
            System.out.println(cont + "- " + listaEmpresa.get(i).getNomEmpresa());
        }

        System.out.println("Ingrese Nro de Empresa: ");
        int c;
        c = sc.nextInt();

        String respc = "s";
        while (respc.equals("s")) {
            System.out.println("INGRESE CLIENTE");
            System.out.println("DNI: ");
            String dni = sc.next();
            System.out.println("APELLIDO Y NOMBRE: ");
            String ayn = sc.next();
            System.out.println("DIRECCION : ");
            String dir = sc.next();

            cli = new Cliente(dni, ayn, dir);
            listaEmpresa.get(c - 1).listaCli.add(cli);

            System.out.println("Continuar: s/n");
            respc = sc.next();
        }
    }
    
    public void AgregarEmpleado() {
        System.out.println("EMPRESAS: ");
        int cont = 0;
        for (int i = 0; i < listaEmpresa.size(); i++) {
            cont = cont + 1;
            System.out.println(cont + "- " + listaEmpresa.get(i).getNomEmpresa());
        }

        System.out.println("Ingrese Nro de Empresa: ");
        int e;
        e = sc.nextInt();

        String respe = "s";
        while (respe.equals("s")) {
            System.out.println("INGRESE EMPLEADO");
            System.out.println("CODIGO: ");
            int codi = sc.nextInt();
            System.out.println("APELLIDO Y NOMBRE: ");
            String ayn = sc.next();
            System.out.println("TIPO : (vendedor - encargado)");
            String tip = sc.next();
            System.out.println("VENTAS: ");
            int vent = sc.nextInt();
            System.out.println("HS EXTRA: ");
            int hsex = sc.nextInt();

            empp = new Empleado(codi, ayn, tip, vent, hsex);
            listaEmpresa.get(e - 1).listaEmp.add(empp);

            System.out.println("Continuar: s/n");
            respe = sc.next();
        }
    }
    
    public void mostrarClientes(){
        System.out.println("EMPRESAS: ");
        int cont = 0;
        for (int i = 0; i < listaEmpresa.size(); i++) {
            cont = cont + 1;
            System.out.println(cont + "- " + listaEmpresa.get(0).getNomEmpresa());
        }

        System.out.println("Ingrese Nro de Empresa: ");
        int mc;
        mc = sc.nextInt();
        
        System.out.println("EMPRESA: ");
        System.out.println(listaEmpresa.get(mc - 1).getNomEmpresa());
        System.out.println("CLIENTES");
        for (int i = 0; i < listaEmpresa.get(mc - 1).listaCli.size(); i++) {
            System.out.println("-" + listaEmpresa.get(mc - 1).listaCli.get(i).getDni()
            + " " + listaEmpresa.get(mc - 1).listaCli.get(i).getAyn()
            + " " + listaEmpresa.get(mc - 1).listaCli.get(i).getDireccion());
        }
    }
    
    public void mostrarEmpleados(){
        System.out.println("EMPRESAS: ");
        int cont = 0;
        for (int i = 0; i < listaEmpresa.size(); i++) {
            cont = cont + 1;
            System.out.println(cont + "- " + listaEmpresa.get(0).getNomEmpresa());
        }

        System.out.println("Ingrese Nro de Empresa: ");
        int me;
        me = sc.nextInt();
        
        System.out.println("EMPRESA: ");
        System.out.println(listaEmpresa.get(me - 1).getNomEmpresa());
        System.out.println("EMPLEADOS");
        for (int i = 0; i < listaEmpresa.get(me - 1).listaEmp.size(); i++) {
            System.out.println("-" + listaEmpresa.get(me - 1).listaEmp.get(i).getCodigo()
            + " " + listaEmpresa.get(me - 1).listaEmp.get(i).getAyn()
            + " " + listaEmpresa.get(me - 1).listaEmp.get(i).getTipo()
            + " " + listaEmpresa.get(me - 1).listaEmp.get(i).getVentasmes()
            + " " + listaEmpresa.get(me - 1).listaEmp.get(i).getHsextra()
            );
        }
    }
    
}
