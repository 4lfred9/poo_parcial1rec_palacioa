package poo_parcial1rec_palacioa;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author 4lfred
 */
public class Empleado {

    private int codigo;
    private String ayn;
    private String tipo;
    private int ventasmes;
    private int hsextra;

    public Empleado() {
    }

    public Empleado(int codigo, String ayn, String tipo, int ventasmes, int hsextra) {
        this.codigo = codigo;
        this.ayn = ayn;
        this.tipo = tipo;
        this.ventasmes = ventasmes;
        this.hsextra = hsextra;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getAyn() {
        return ayn;
    }

    public void setAyn(String ayn) {
        this.ayn = ayn;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getVentasmes() {
        return ventasmes;
    }

    public void setVentasmes(int ventasmes) {
        this.ventasmes = ventasmes;
    }

    public int getHsextra() {
        return hsextra;
    }

    public void setHsextra(int hsextra) {
        this.hsextra = hsextra;
    }

    public int calculaSalarioBruto(String tipo, int ventasmes, int hsextra) {
        if(tipo == "vendedor"){
            int salarioBase = 10000;
        }
        
        if(tipo == "encargado"){
            int salarioBase = 15000;
        }
        
        return 0;
    }

    public float calcularSalarioNeto(int salarioBruto) {
        float salarioNeto = 0;
        if (salarioBruto < 10000) {
            salarioNeto = salarioBruto;
        } else {
            if (salarioBruto >= 10000 && salarioBruto < 15000) {
                salarioNeto = salarioBruto - salarioBruto * 16 / 100;
            } else {
                if (salarioBruto >= 15000) {
                    salarioNeto = salarioBruto + salarioBruto * 0.18f;
                }
            }
        }
        if (salarioBruto < 0) {
            return 0;
        } else {
            return salarioNeto;
        }
    }

}
