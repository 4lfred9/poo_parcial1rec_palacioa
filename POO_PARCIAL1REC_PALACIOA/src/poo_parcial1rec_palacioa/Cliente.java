
package poo_parcial1rec_palacioa;

/**
 *
 * @author 4lfred
 */
public class Cliente {
    private String dni;
    private String ayn;
    private String direccion;

    public Cliente() {
    }

    public Cliente(String dni, String ayn, String direccion) {
        this.dni = dni;
        this.ayn = ayn;
        this.direccion = direccion;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getAyn() {
        return ayn;
    }

    public void setAyn(String ayn) {
        this.ayn = ayn;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    public void mostrarCli(){
        System.out.println("--------------DNI: " + dni);
        System.out.println("APELLIDO Y NOMBRE: " + ayn);
        System.out.println("--------DIRECCION: " + direccion);
    }
}
