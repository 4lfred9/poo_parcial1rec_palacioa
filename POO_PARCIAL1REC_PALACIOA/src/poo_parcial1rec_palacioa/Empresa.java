
package poo_parcial1rec_palacioa;

import java.util.ArrayList;

/**
 *
 * @author 4lfred
 */
public class Empresa {
    //private Empleado emp;
    //private Cliente cli;
    ArrayList<Cliente> listaCli = new ArrayList();
    ArrayList<Empleado> listaEmp = new ArrayList();
    private String nomEmpresa;

    public ArrayList<Cliente> getListaCli() {
        return listaCli;
    }

    public void setListaCli(ArrayList<Cliente> listaCli) {
        this.listaCli = listaCli;
    }

    public ArrayList<Empleado> getListaEmp() {
        return listaEmp;
    }

    public void setListaEmp(ArrayList<Empleado> listaEmp) {
        this.listaEmp = listaEmp;
    }

    public String getNomEmpresa() {
        return nomEmpresa;
    }

    public void setNomEmpresa(String nomEmpresa) {
        this.nomEmpresa = nomEmpresa;
    }

    public void mostrarEmpresas(){
        System.out.println("----EMPRESAS-----");
        System.out.println(nomEmpresa);
    }
         
    }

   
   
