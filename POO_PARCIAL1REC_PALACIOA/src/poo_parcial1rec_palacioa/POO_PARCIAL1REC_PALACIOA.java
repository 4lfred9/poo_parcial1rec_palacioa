
package poo_parcial1rec_palacioa;

import java.util.Optional;
import java.util.Scanner;

/**
 *
 * @author 4lfred
 */
public class POO_PARCIAL1REC_PALACIOA {

    public static void main(String[] args) {
        
        ListaEmpresa lempresa = new ListaEmpresa();
        Scanner sc = new Scanner(System.in);
        
        int op = 0;
        do{
            System.out.println("-----MENU DE OPCIONES-----");
            System.out.println("1. INGRESAR DATO DE EMPRESA");
            System.out.println("2. LISTA DE EMPRESAS");
            System.out.println("3. AGREGAR CLIENTE A EMPRESA");
            System.out.println("4. AGREGAR EMPLEADO A EMPRESA");
            System.out.println("5. CLIENTE + EMPRESA");
            System.out.println("6. EMPLEADO + EMPRESA");
            System.out.println("7. SALIR");
            op = sc.nextInt();
            switch (op){
                case 1:
                    lempresa.agregarEmpresa();
                    break;
                case 2:
                    lempresa.listarEmpresa();
                    break;
                case 3:
                    lempresa.AgregarCliente();
                    break;
                case 4:
                    lempresa.AgregarEmpleado();
                    break;
                case 5:
                    lempresa.mostrarClientes();
                    break;
                case 6:
                    lempresa.mostrarEmpleados();
                    break;
                case 7:
                    break;
            }
        }while(op != 7);
    }
    
}
